import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'mt-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public readonly test$: Observable<{}[]>;

  public constructor(db: AngularFirestore) {
    this.test$ = db.collection('test').valueChanges();
  }
}
