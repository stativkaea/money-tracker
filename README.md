Run `yarn start` for a dev server. Navigate to `http://localhost:4200/`

Run `yarn build` to build the project

Run `yarn deploy` to deploy the app to Firebase hosting

Run `yarn test` to execute the unit tests

Run `yarn e2e` to execute the end-to-end tests
