import * as functions from 'firebase-functions';

export const handleTransaction2 = functions.firestore
  .document('/transactions/{transactionId}')
  .onUpdate(({ after, before }, context) => {
    console.log('before', before.data());
    console.log('after', after.data());
    console.log('context', JSON.stringify(context));

    return null;
  });

async function handleExpenceChange(): Promise<void> {
  return undefined;
}

export const handleTransaction22 = functions.firestore
  .document('/transactions/{transactionId}')
  .onWrite(({ after, before }) => {
    const amountAfter = after.get('amount') || 0;
    console.log('amountAfter', amountAfter);
    const amountBefore = before.get('amount') || 0;
    console.log('amountBefore', amountBefore);

    // TODO handle account change
    if (amountAfter === amountBefore) {
      return null;
    }

    const fromAccountName = after.get('fromAccount');
    console.log('fromAccount id', fromAccountName.id);
    console.log('fromAccountName', fromAccountName.get('name'));
    const toAccountName: string | undefined = after.get('toAccount');
    console.log('toAccountName', toAccountName);

    const fromAccount = after.ref.firestore.doc(`/accounts/${fromAccountName}`);

    return fromAccount
      .get()
      .then(data => data.get('balance'))
      .then(balance => {
        console.log('balance was ', balance);
        console.log('amountBefore ', amountBefore);
        console.log('amountAfter ', amountAfter);
        console.log('new balance is ', balance + amountBefore - amountAfter);

        const _account = after.get('fromAccount');
        console.log('account is', _account);

        return fromAccount.set({ balance: balance + amountBefore - amountAfter }, { merge: true });
      });
  });
